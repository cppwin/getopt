// CppWin.GetOpt.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "CppWin.GetOpt.h"


// This is an example of an exported variable
CPPWINGETOPT_API int nCppWinGetOpt=0;

// This is an example of an exported function.
CPPWINGETOPT_API int fnCppWinGetOpt(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see CppWin.GetOpt.h for the class definition
CCppWinGetOpt::CCppWinGetOpt()
{
    return;
}


// https://github.com/mono/mono/blob/master/mcs/class/Mono.Options/Mono.Options/Options.cs
namespace CppWin
{
	int GetOpt::getopt(int argc, char *const *argv, const char *options)
	{
		// ???
		// return ::getopt(argc, argv, options);
		return 0;
	}
}
