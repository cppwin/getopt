// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CPPWINGETOPT_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CPPWINGETOPT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef CPPWINGETOPT_EXPORTS
#define CPPWINGETOPT_API __declspec(dllexport)
#else
#define CPPWINGETOPT_API __declspec(dllimport)
#endif

// This class is exported from the CppWin.GetOpt.dll
class CPPWINGETOPT_API CCppWinGetOpt {
public:
	CCppWinGetOpt(void);
	// TODO: add your methods here.
};

extern CPPWINGETOPT_API int nCppWinGetOpt;

CPPWINGETOPT_API int fnCppWinGetOpt(void);


// https://msdn.microsoft.com/en-us/library/ms235636.aspx
// https://github.com/mono/mono/blob/master/mcs/class/Mono.Options/Mono.Options/Options.cs
namespace CppWin
{
	class GetOpt
	{
	public:
		static CPPWINGETOPT_API int getopt(int argc, char *const *argv, const char *options);
	};
}

